@echo off
if "%VSCMD_VER%"=="" (
	:: Setting the VSCMD_START_DIR variable to the current directory otherwise VsDevCmd.bat may change the current directory (see https://developercommunity.visualstudio.com/content/problem/26780/vsdevcmdbat-changes-the-current-working-directory.html)
	set VSCMD_START_DIR=%CD%
	call "C:\Program Files\Microsoft Visual Studio\2022\Professional\Common7\Tools\VsDevCmd.bat"
)

dotnet restore ..\T8\Lacuna.T8.csproj
msbuild /t:pack /p:Configuration=Release /p:owners=Lacuna ..\T8\Lacuna.T8.csproj
move ..\T8\bin\Release\*.nupkg .

:end
pause
