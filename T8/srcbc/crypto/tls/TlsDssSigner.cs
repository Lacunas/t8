﻿using System;

using Lacuna.T8.BC.Crypto.Digests;
using Lacuna.T8.BC.Crypto.Signers;

namespace Lacuna.T8.BC.Crypto.Tls
{
	internal class TlsDssSigner
		: DsaDigestSigner
	{
		internal TlsDssSigner()
        	: base(new DsaSigner(), new Sha1Digest())
		{
		}
	}
}
