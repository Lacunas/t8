﻿using System;

using Lacuna.T8.BC.Crypto.Encodings;
using Lacuna.T8.BC.Crypto.Engines;
using Lacuna.T8.BC.Crypto.Signers;

namespace Lacuna.T8.BC.Crypto.Tls
{
	internal class TlsRsaSigner
		: GenericSigner
	{
		internal TlsRsaSigner()
        	: base(new Pkcs1Encoding(new RsaBlindedEngine()), new CombinedHash())
		{
		}
	}
}
