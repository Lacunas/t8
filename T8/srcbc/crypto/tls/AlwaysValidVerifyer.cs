﻿using System;

using Lacuna.T8.BC.Asn1.X509;

namespace Lacuna.T8.BC.Crypto.Tls
{
	/// <remarks>
	/// A certificate verifyer, that will always return true.
	/// <pre>
	/// DO NOT USE THIS FILE UNLESS YOU KNOW EXACTLY WHAT YOU ARE DOING.
	/// </pre>
	/// </remarks>
	internal class AlwaysValidVerifyer
		: ICertificateVerifyer
	{
		/// <summary>Return true.</summary>
		public bool IsValid(
			X509CertificateStructure[] certs)
		{
			return true;
		}
	}
}
