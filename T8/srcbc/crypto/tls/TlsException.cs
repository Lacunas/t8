﻿using System;

namespace Lacuna.T8.BC.Crypto.Tls
{
	internal class TlsException : Exception
	{
		public TlsException() : base() { }
		public TlsException(string message) : base(message) { }
		public TlsException(string message, Exception exception) : base(message, exception) { }
	}
}
