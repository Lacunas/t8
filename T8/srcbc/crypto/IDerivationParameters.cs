﻿using System;

namespace Lacuna.T8.BC.Crypto
{
    /**
     * Parameters for key/byte stream derivation classes
     */
    internal interface IDerivationParameters
    {
    }
}
