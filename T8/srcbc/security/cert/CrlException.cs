﻿using System;

namespace Lacuna.T8.BC.Security.Certificates
{
	internal class CrlException : GeneralSecurityException
	{
		public CrlException() : base() { }
		public CrlException(string msg) : base(msg) {}
		public CrlException(string msg, Exception e) : base(msg, e) {}
	}
}