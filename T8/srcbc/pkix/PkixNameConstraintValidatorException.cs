﻿using System;

namespace Lacuna.T8.BC.Pkix
{
    internal class PkixNameConstraintValidatorException : Exception
    {
        public PkixNameConstraintValidatorException(String msg)
            : base(msg)
        {
        }
    }
}
