﻿using System;
using System.IO;

namespace Lacuna.T8.BC.Security
{
	internal class PasswordException
		: IOException
	{
		public PasswordException(
			string message)
			: base(message)
		{
		}

		public PasswordException(
			string		message,
			Exception	exception)
			: base(message, exception)
		{
		}
	}
}
