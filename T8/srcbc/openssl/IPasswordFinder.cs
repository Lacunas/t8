﻿using System;

namespace Lacuna.T8.BC.OpenSsl
{
	internal interface IPasswordFinder
	{
		char[] GetPassword();
	}
}
