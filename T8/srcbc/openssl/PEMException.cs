﻿using System;
using System.IO;

namespace Lacuna.T8.BC.OpenSsl
{
	internal class PemException
		: IOException
	{
		public PemException(
			string message)
			: base(message)
		{
		}

		public PemException(
			string		message,
			Exception	exception)
			: base(message, exception)
		{
		}
	}
}
