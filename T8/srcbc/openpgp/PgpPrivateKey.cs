﻿using System;

using Lacuna.T8.BC.Crypto;

namespace Lacuna.T8.BC.Bcpg.OpenPgp
{
	/// <remarks>General class to contain a private key for use with other OpenPGP objects.</remarks>
    internal class PgpPrivateKey
    {
        private readonly long keyId;
        private readonly AsymmetricKeyParameter privateKey;

		/// <summary>
		/// Create a PgpPrivateKey from a regular private key and the ID of its
		/// associated public key.
		/// </summary>
		/// <param name="privateKey">Private key to use.</param>
		/// <param name="keyId">ID of the corresponding public key.</param>
		public PgpPrivateKey(
            AsymmetricKeyParameter	privateKey,
            long					keyId)
        {
			if (!privateKey.IsPrivate)
				throw new ArgumentException("Expected a private key", "privateKey");

			this.privateKey = privateKey;
            this.keyId = keyId;
        }

		/// <summary>The keyId associated with the contained private key.</summary>
        public long KeyId
        {
			get { return keyId; }
        }

		/// <summary>The contained private key.</summary>
        public AsymmetricKeyParameter Key
        {
			get { return privateKey; }
        }
    }
}
