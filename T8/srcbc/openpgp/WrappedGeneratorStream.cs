﻿using System.IO;

using Lacuna.T8.BC.Asn1.Utilities;

namespace Lacuna.T8.BC.Bcpg.OpenPgp
{
	internal class WrappedGeneratorStream
		: FilterStream
	{
		private readonly IStreamGenerator gen;

		public WrappedGeneratorStream(
			IStreamGenerator	gen,
			Stream				str)
			: base(str)
		{
			this.gen = gen;
		}

		public override void Close()
		{
			gen.Close();
		}
	}
}
