﻿using System;

namespace Lacuna.T8.BC.Bcpg.OpenPgp
{
	/// <remarks>Generic exception class for PGP encoding/decoding problems.</remarks>
	internal class PgpException
		: Exception
	{
		public PgpException() : base() {}
		public PgpException(string message) : base(message) {}
		public PgpException(string message, Exception exception) : base(message, exception) {}

		[Obsolete("Use InnerException property")]
		public Exception UnderlyingException
		{
			get { return InnerException; }
		}
	}
}
