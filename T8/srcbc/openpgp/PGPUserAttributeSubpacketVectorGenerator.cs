﻿using System;
using System.Collections;

using Lacuna.T8.BC.Bcpg.Attr;

namespace Lacuna.T8.BC.Bcpg.OpenPgp
{
	internal class PgpUserAttributeSubpacketVectorGenerator
	{
		private ArrayList list = new ArrayList();

		public virtual void SetImageAttribute(
			ImageAttrib.Format	imageType,
			byte[]				imageData)
		{
			if (imageData == null)
				throw new ArgumentException("attempt to set null image", "imageData");

			list.Add(new ImageAttrib(imageType, imageData));
		}

		public virtual PgpUserAttributeSubpacketVector Generate()
		{
			return new PgpUserAttributeSubpacketVector(
				(UserAttributeSubpacket[]) list.ToArray(typeof(UserAttributeSubpacket)));
		}
	}
}
