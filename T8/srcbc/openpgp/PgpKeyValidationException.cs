﻿using System;

namespace Lacuna.T8.BC.Bcpg.OpenPgp
{
	/// <remarks>
	/// Thrown if the key checksum is invalid.
	/// </remarks>
	internal class PgpKeyValidationException
		: PgpException
	{
		public PgpKeyValidationException() : base() {}
		public PgpKeyValidationException(string message) : base(message) {}
		public PgpKeyValidationException(string message, Exception exception) : base(message, exception) {}
	}
}
