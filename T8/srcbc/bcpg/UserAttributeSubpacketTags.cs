﻿namespace Lacuna.T8.BC.Bcpg
{
    /**
    * Basic PGP user attribute sub-packet tag types.
    */
    public enum UserAttributeSubpacketTag
    {
        ImageAttribute = 1
    }
}
