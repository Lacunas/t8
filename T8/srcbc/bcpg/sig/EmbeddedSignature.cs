﻿using System;

namespace Lacuna.T8.BC.Bcpg.Sig
{
	/**
	 * Packet embedded signature
	 */
	internal class EmbeddedSignature
		: SignatureSubpacket
	{
		public EmbeddedSignature(
			bool	critical,
			byte[]	data)
			: base(SignatureSubpacketTag.EmbeddedSignature, critical, data)
		{
		}
	}
}
