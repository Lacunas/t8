﻿using System;

using Lacuna.T8.BC.Asn1;
using Lacuna.T8.BC.Asn1.Ocsp;
using Lacuna.T8.BC.Asn1.X509;
using Lacuna.T8.BC.Crypto;
using Lacuna.T8.BC.Security;
using Lacuna.T8.BC.X509;

namespace Lacuna.T8.BC.Ocsp
{
	/**
	 * Carrier for a ResponderID.
	 */
	internal class RespID
	{
		internal readonly ResponderID id;

		public RespID(
			ResponderID id)
		{
			this.id = id;
		}

		public RespID(
			X509Name name)
		{
		    try
		    {
		        this.id = new ResponderID(name);
		    }
		    catch (Exception e)
		    {
		        throw new ArgumentException("can't decode name.", e);
		    }
		}

		public RespID(
			AsymmetricKeyParameter publicKey)
		{
			try
			{
				IDigest digest = DigestUtilities.GetDigest("SHA1");

				SubjectPublicKeyInfo info = SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(publicKey);

				byte[] encoded = info.PublicKeyData.GetBytes();
				digest.BlockUpdate(encoded, 0, encoded.Length);

				byte[] hash = DigestUtilities.DoFinal(digest);

				Asn1OctetString keyHash = new DerOctetString(hash);

				this.id = new ResponderID(keyHash);
			}
			catch (Exception e)
			{
				throw new OcspException("problem creating ID: " + e, e);
			}
		}

		public ResponderID ToAsn1Object()
		{
			return id;
		}

		public override bool Equals(
			object obj)
		{
			if (obj == this)
				return true;

			RespID other = obj as RespID;

			if (other == null)
				return false;

			return id.Equals(other.id);
		}

		public override int GetHashCode()
		{
			return id.GetHashCode();
		}
	}
}
