﻿using System;

namespace Lacuna.T8.BC.Ocsp
{
	/**
	 * wrapper for the UnknownInfo object
	 */
	internal class UnknownStatus
		: CertificateStatus
	{
		public UnknownStatus()
		{
		}
	}
}
