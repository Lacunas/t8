﻿using System;
using System.Collections;
using System.IO;

using Lacuna.T8.BC.Asn1;
using Lacuna.T8.BC.Asn1.Ocsp;
using Lacuna.T8.BC.Asn1.X509;
using Lacuna.T8.BC.X509;

namespace Lacuna.T8.BC.Ocsp
{
	internal class Req
		: X509ExtensionBase
	{
		private Request req;

		public Req(
			Request req)
		{
			this.req = req;
		}

		public CertificateID GetCertID()
		{
			return new CertificateID(req.ReqCert);
		}

		public X509Extensions SingleRequestExtensions
		{
			get { return req.SingleRequestExtensions; }
		}

		protected override X509Extensions GetX509Extensions()
		{
			return SingleRequestExtensions;
		}
	}
}
