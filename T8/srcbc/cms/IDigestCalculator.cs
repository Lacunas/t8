﻿using System;

namespace Lacuna.T8.BC.Cms
{
	internal interface IDigestCalculator
	{
		byte[] GetDigest();
	}
}
