﻿using System;

namespace Lacuna.T8.BC.Cms
{
    internal class CmsException
        : Exception
    {
		public CmsException()
		{
		}

		public CmsException(
			string name)
			: base(name)
        {
        }

		public CmsException(
			string		name,
			Exception	e)
			: base(name, e)
        {
        }
    }
}
