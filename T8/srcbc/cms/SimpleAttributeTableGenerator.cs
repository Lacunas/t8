﻿using System;
using System.Collections;

using Lacuna.T8.BC.Asn1.Cms;

namespace Lacuna.T8.BC.Cms
{
	/**
	 * Basic generator that just returns a preconstructed attribute table
	 */
	internal class SimpleAttributeTableGenerator
		: CmsAttributeTableGenerator
	{
		private readonly AttributeTable attributes;

		public SimpleAttributeTableGenerator(
			AttributeTable attributes)
		{
			this.attributes = attributes;
		}

		public virtual AttributeTable GetAttributes(
			IDictionary parameters)
		{
			return attributes;
		}
	}
}
