﻿using System.IO;

using Lacuna.T8.BC.Utilities.IO;

namespace Lacuna.T8.BC.Asn1
{
    internal abstract class LimitedInputStream
        : BaseInputStream
    {
        protected readonly Stream _in;

        internal LimitedInputStream(
            Stream inStream)
        {
            this._in = inStream;
        }

		protected virtual void SetParentEofDetect(bool on)
        {
            if (_in is IndefiniteLengthInputStream)
            {
                ((IndefiniteLengthInputStream)_in).SetEofOn00(on);
            }
        }
    }
}
