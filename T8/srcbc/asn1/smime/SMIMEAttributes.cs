﻿using Lacuna.T8.BC.Asn1;
using Lacuna.T8.BC.Asn1.Pkcs;

namespace Lacuna.T8.BC.Asn1.Smime
{
    internal abstract class SmimeAttributes
    {
        public static readonly DerObjectIdentifier SmimeCapabilities = PkcsObjectIdentifiers.Pkcs9AtSmimeCapabilities;
        public static readonly DerObjectIdentifier EncrypKeyPref = PkcsObjectIdentifiers.IdAAEncrypKeyPref;
    }
}
