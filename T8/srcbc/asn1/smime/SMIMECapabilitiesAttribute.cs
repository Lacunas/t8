﻿using Lacuna.T8.BC.Asn1;
using Lacuna.T8.BC.Asn1.X509;

namespace Lacuna.T8.BC.Asn1.Smime
{
    internal class SmimeCapabilitiesAttribute
        : AttributeX509
    {
        public SmimeCapabilitiesAttribute(
            SmimeCapabilityVector capabilities)
            : base(SmimeAttributes.SmimeCapabilities,
                    new DerSet(new DerSequence(capabilities.ToAsn1EncodableVector())))
        {
        }
    }
}
