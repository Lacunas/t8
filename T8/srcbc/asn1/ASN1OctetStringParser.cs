﻿using System.IO;

namespace Lacuna.T8.BC.Asn1
{
	internal interface Asn1OctetStringParser
		: IAsn1Convertible
	{
		Stream GetOctetStream();
	}
}
