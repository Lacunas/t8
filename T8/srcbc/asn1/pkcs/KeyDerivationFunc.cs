﻿using Lacuna.T8.BC.Asn1;
using Lacuna.T8.BC.Asn1.X509;

namespace Lacuna.T8.BC.Asn1.Pkcs
{
    internal class KeyDerivationFunc
        : AlgorithmIdentifier
    {
        internal KeyDerivationFunc(Asn1Sequence seq)
			: base(seq)
        {
        }

		internal KeyDerivationFunc(
			DerObjectIdentifier	id,
			Asn1Encodable		parameters)
			: base(id, parameters)
        {
        }
    }
}
