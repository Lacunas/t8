﻿using System;

using Lacuna.T8.BC.Asn1;
using Lacuna.T8.BC.Asn1.X509;

namespace Lacuna.T8.BC.Asn1.Pkcs
{
    internal class EncryptionScheme
        : AlgorithmIdentifier
    {
        private readonly Asn1Object	objectID, obj;

		internal EncryptionScheme(
			Asn1Sequence seq)
			: base(seq)
        {
            objectID = (Asn1Object) seq[0];
            obj = (Asn1Object) seq[1];
        }

		public Asn1Object Asn1Object
		{
			get { return obj; }
		}

		public override Asn1Object ToAsn1Object()
        {
			return new DerSequence(objectID, obj);
        }
    }
}
