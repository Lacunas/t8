﻿using Lacuna.T8.BC.Asn1;

namespace Lacuna.T8.BC.Asn1.Misc
{
    internal class NetscapeRevocationUrl
        : DerIA5String
    {
        public NetscapeRevocationUrl(DerIA5String str)
			: base(str.GetString())
        {
        }

        public override string ToString()
        {
            return "NetscapeRevocationUrl: " + this.GetString();
        }
    }
}
