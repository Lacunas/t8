﻿namespace Lacuna.T8.BC.Asn1
{
    /**
     * basic interface for Der string objects.
     */
    internal interface IAsn1String
    {
        string GetString();
    }
}
