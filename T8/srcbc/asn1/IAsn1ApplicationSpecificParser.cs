﻿using System;

namespace Lacuna.T8.BC.Asn1
{
	internal interface IAsn1ApplicationSpecificParser
    	: IAsn1Convertible
	{
    	IAsn1Convertible ReadObject();
	}
}
