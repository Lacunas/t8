﻿namespace Lacuna.T8.BC.Asn1
{
	internal interface IAsn1Convertible
	{
		Asn1Object ToAsn1Object();
	}
}
