﻿using System;

namespace Lacuna.T8.BC.Asn1.X509
{
	internal class X509Attributes
	{
		public static readonly DerObjectIdentifier RoleSyntax = new DerObjectIdentifier("2.5.4.72");
	}
}
