﻿using System;
using System.Collections;

namespace Lacuna.T8.BC.Utilities.Collections
{
	internal interface ISet
		: ICollection
	{
		void Add(object o);
		void AddAll(IEnumerable e);
		void Clear();
		bool Contains(object o);
		bool IsEmpty { get; }
		void Remove(object o);
		void RemoveAll(IEnumerable e);
	}
}
