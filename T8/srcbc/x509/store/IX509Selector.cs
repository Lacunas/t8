﻿using System;

namespace Lacuna.T8.BC.X509.Store
{
	internal interface IX509Selector
		: ICloneable
	{
		bool Match(object obj);
	}
}
