﻿using System;

namespace Lacuna.T8.BC.X509.Store
{
	internal class NoSuchStoreException
		: X509StoreException
	{
		public NoSuchStoreException()
		{
		}

		public NoSuchStoreException(
			string message)
			: base(message)
		{
		}

		public NoSuchStoreException(
			string		message,
			Exception	e)
			: base(message, e)
		{
		}
	}
}
