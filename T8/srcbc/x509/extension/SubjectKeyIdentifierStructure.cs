﻿using System;

using Lacuna.T8.BC.Asn1;
using Lacuna.T8.BC.Asn1.X509;
using Lacuna.T8.BC.Crypto;
using Lacuna.T8.BC.Security.Certificates;

namespace Lacuna.T8.BC.X509.Extension
{
	/**
	 * A high level subject key identifier.
	 */
	internal class SubjectKeyIdentifierStructure
		: SubjectKeyIdentifier
	{
//		private AuthorityKeyIdentifier authKeyID;

		/**
		 * Constructor which will take the byte[] returned from getExtensionValue()
		 * 
		 * @param encodedValue a DER octet encoded string with the extension structure in it.
		 * @throws IOException on parsing errors.
		 */
		public SubjectKeyIdentifierStructure(
			Asn1OctetString encodedValue)
			: base((Asn1OctetString) X509ExtensionUtilities.FromExtensionValue(encodedValue))
		{
		}

		private static Asn1OctetString FromPublicKey(
			AsymmetricKeyParameter pubKey)
		{
			try
			{
				SubjectPublicKeyInfo info = SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(pubKey);

				return (Asn1OctetString) new SubjectKeyIdentifier(info).ToAsn1Object();
			}
			catch (Exception e)
			{
				throw new CertificateParsingException("Exception extracting certificate details: " + e.ToString());
			}
		}

		public SubjectKeyIdentifierStructure(
			AsymmetricKeyParameter pubKey)
			: base(FromPublicKey(pubKey))
		{
		}
	}
}
