﻿using System;

namespace Lacuna.T8.text {
    /// <summary>
    /// Interface for a text element to which other objects can be added.
    /// </summary>
    /// <seealso cref="T:Lacuna.T8.text.Phrase"/>
    /// <seealso cref="T:Lacuna.T8.text.Paragraph"/>
    /// <seealso cref="T:Lacuna.T8.text.Section"/>
    /// <seealso cref="T:Lacuna.T8.text.ListItem"/>
    /// <seealso cref="T:Lacuna.T8.text.Chapter"/>
    /// <seealso cref="T:Lacuna.T8.text.Anchor"/>
    /// <seealso cref="T:Lacuna.T8.text.Cell"/>
    internal interface ITextElementArray : IElement {
        /// <summary>
        /// Adds an object to the TextElementArray.
        /// </summary>
        /// <param name="o">an object that has to be added</param>
        /// <returns>true if the addition succeeded; false otherwise</returns>
        bool Add(Object o);
    }
}