﻿using System;
using System.IO;
using System.Text;
using System.Globalization;

namespace Lacuna.T8.text.pdf {
    
	/// <summary>
	/// Lacuna Improvement on ByteBuffer for using MemoryStream instead of byte arrays resizing
	/// </summary>
    internal class ByteBufferLacuna : Stream {

		public MemoryStream Stream { get; }
    
        /** Creates new ByteBuffer with capacity 128 */
        public ByteBufferLacuna() {
			Stream = new MemoryStream();
		}

		public ByteBufferLacuna Append(byte[] b, int off, int len) {
			if ((off < 0) || (off > b.Length) || (len < 0) ||
				((off + len) > b.Length) || ((off + len) < 0) || len == 0) {
				return this;
			}
			Stream.Write(b, off, len);
			return this;
		}

		public ByteBufferLacuna Append(byte b) {
			return Append_i(b);
		}

		public ByteBufferLacuna Append_i(int b) {
			Stream.WriteByte((byte)b);
			return this;
		}

		public override bool CanRead => Stream.CanRead;
		public override bool CanSeek => Stream.CanSeek;
		public override bool CanWrite => Stream.CanWrite;
		public override long Length => Stream.Length;
		public override long Position {
			get => Stream.Position;
			set => Stream.Position = value;
        }
    
        public override void Flush() {
			Stream.Flush();
        }
    
        public override int Read(byte[] buffer, int offset, int count) {
			return Stream.Read(buffer, offset, count);
        }
    
        public override long Seek(long offset, SeekOrigin origin) {
			return Stream.Seek(offset, origin);
        }
    
        public override void SetLength(long value) {
			Stream.SetLength(value);
        }
    
        public override void Write(byte[] buffer, int offset, int count) {
            Append(buffer, offset, count);
        }
    
        public override void WriteByte(byte value) {
            Append(value);
        }

		public void WriteAt(byte[] buffer, int offset, int count, int position, bool recoverPosition = false) {
			var curPos = Position;
			if (position != curPos) {
				Position = position;
			}
			Write(buffer, offset, count);
			if (recoverPosition) {
				Position = curPos;
			}
		}

		public WriteAtStream CreateWriteAtInstance(int position) {
			return new WriteAtStream(this, position);
		}
    }

	internal class WriteAtStream : Stream {
		private readonly ByteBufferLacuna stream;
		private readonly int position;
		public long Written { get; private set; }

		public override bool CanRead => false;
		public override bool CanSeek => stream.CanSeek;
		public override bool CanWrite => stream.CanWrite;
		public override long Length => Written;
		public override long Position {
			get => stream.Position;
			set => stream.Position = value;
		}

		public WriteAtStream(ByteBufferLacuna stream, int position) {
			this.stream = stream;
			this.position = position;
		}

		public override void Flush() {
			stream.Flush();
		}

		public override long Seek(long offset, SeekOrigin origin) {
			return stream.Seek(offset, origin);
		}

		public override void SetLength(long value) {
			stream.SetLength(value);
		}

		public override int Read(byte[] buffer, int offset, int count) {
			throw new NotSupportedException();
		}

		public override void Write(byte[] buffer, int offset, int count) {
			stream.WriteAt(buffer, offset, count, position);
			Written += count;
		}
	}
}
